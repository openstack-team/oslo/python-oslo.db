python-oslo.db (17.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Mar 2025 09:45:53 +0100

python-oslo.db (17.2.0-2) experimental; urgency=medium

  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 14:57:00 +0100

python-oslo.db (17.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Feb 2025 14:11:28 +0100

python-oslo.db (16.0.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090567).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 09:09:40 +0100

python-oslo.db (16.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:50:55 +0200

python-oslo.db (16.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 15:35:17 +0200

python-oslo.db (15.0.0-3) unstable; urgency=medium

  * Remove extraneous python3-mock dependency (Closes: #1069243).

 -- Thomas Goirand <zigo@debian.org>  Fri, 19 Apr 2024 10:36:38 +0200

python-oslo.db (15.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:32:35 +0200

python-oslo.db (15.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 21:38:33 +0100

python-oslo.db (14.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 13:17:37 +0200

python-oslo.db (14.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Sep 2023 12:21:02 +0200

python-oslo.db (14.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed sqlalchemy-migrate (b-)depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 12:16:58 +0200

python-oslo.db (12.3.1-3) unstable; urgency=medium

  * Cleans better (Closes: #1046291).

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Aug 2023 18:33:49 +0200

python-oslo.db (12.3.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 11:25:10 +0200

python-oslo.db (12.3.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed min version of python3-sqlalchemy.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 10:29:45 +0100

python-oslo.db (12.1.0-3) unstable; urgency=medium

  * Do not run test_enginefacade.* tests, as they work, but stay stuck and
    never finish (Closes: #1031481).

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Feb 2023 08:58:57 +0100

python-oslo.db (12.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 16:54:04 +0200

python-oslo.db (12.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 31 Aug 2022 09:10:31 +0200

python-oslo.db (12.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 16:08:07 +0200

python-oslo.db (11.2.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 15:49:08 +0100

python-oslo.db (11.2.0-2) experimental; urgency=medium

  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Feb 2022 22:44:10 +0100

python-oslo.db (11.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 11:31:59 +0100

python-oslo.db (11.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 16:22:40 +0200

python-oslo.db (11.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Updated (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 13:57:25 +0200

python-oslo.db (8.5.1-1) unstable; urgency=medium

  * New upstream release

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 20 Aug 2021 12:14:57 +0200

python-oslo.db (8.5.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 09:03:12 +0200

python-oslo.db (8.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 09:06:53 +0100

python-oslo.db (8.4.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed d/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 08:35:14 +0200

python-oslo.db (8.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 15:34:47 +0200

python-oslo.db (8.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-os-testr from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 21:03:11 +0200

python-oslo.db (8.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 21:58:51 +0200

python-oslo.db (8.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends after Buster is released.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 14:42:10 +0200

python-oslo.db (5.0.2-3) unstable; urgency=medium

  * Removed autopkgtest, as it's using Python 2 (Closes: #943221).

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 12:11:12 +0200

python-oslo.db (5.0.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 00:16:26 +0200

python-oslo.db (5.0.2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.
  * Add python3-sphinxcontrib.apidoc as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Sep 2019 12:10:39 +0200

python-oslo.db (4.44.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 01:43:44 +0200

python-oslo.db (4.44.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Removed Resolve_SAWarning_in_Query.soft_delete.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Mar 2019 21:13:18 +0100

python-oslo.db (4.40.0-3) unstable; urgency=medium

  * Add Resolve_SAWarning_in_Query.soft_delete.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 21 Feb 2019 16:24:27 +0100

python-oslo.db (4.40.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 22:53:21 +0200

python-oslo.db (4.40.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed patches applied upstream:
    - Ignore_use_tpool_option.patch
    - fix-python3.7-async-reserved-keyword.patch

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Aug 2018 18:34:48 +0200

python-oslo.db (4.33.1-2) unstable; urgency=medium

  [ Michal Arbet ]
  * Add patch fix-python3.7-async-reserved-keyword.patch
  * Add removal of .eggs to dh_clean
  * Update uploaders field in d/control
  * Update d/copyright

  [ Thomas Goirand ]
  * Modernize debian/rules and use pkgos-dh_auto_{install,test}.
  * Use Python 3 to build the doc.
  * Use team+openstack@tracker.debian.org as maintainer.

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Jul 2018 12:11:29 +0000

python-oslo.db (4.33.1-1) unstable; urgency=medium

  * New upstream release.
  * Bumped required version of oslotest b-d.
  * Add Ignore_use_tpool_option.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 May 2018 09:21:31 +0200

python-oslo.db (4.33.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:23:24 +0000

python-oslo.db (4.33.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Feb 2018 08:14:22 +0000

python-oslo.db (4.25.0-5) unstable; urgency=medium

  * Team upload.

  [ Michal Arbet ]
  * Added build dependency python{,3}-testscenarios
  * Edit Vcs fields to salsa.debian.org

  [ Ondřej Nový ]
  * Standards-Version is 4.1.3 now (no change)

 -- Ondřej Nový <onovy@debian.org>  Tue, 16 Jan 2018 13:56:54 +0100

python-oslo.db (4.25.0-4) unstable; urgency=medium

  * Added auto-pkg-tests from Ubuntu (Closes: #880602).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 21:33:36 +0100

python-oslo.db (4.25.0-3) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed watch file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 22:23:20 +0000

python-oslo.db (4.25.0-2) experimental; urgency=medium

  * Removed transition packages.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 Sep 2017 19:02:29 +0000

python-oslo.db (4.25.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.

  [ Thomas Goirand ]
  * New upstream release.
  * Reviewed (build-)depends for this release.
  * Using debian/pike in gbp.conf.

  [ Daniel Baumann ]
  * Updating standards version to 4.1.0.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Aug 2017 19:58:56 +0000

python-oslo.db (4.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 08:57:27 +0000

python-oslo.db (4.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Apr 2016 14:22:03 +0200

python-oslo.db (4.6.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https).
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed debian/copyright ordering.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2016 12:11:15 +0000

python-oslo.db (4.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Drop python{3,}-mysqldb build-depends.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 02:11:17 +0000

python-oslo.db (4.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed MANIFEST.in patch.
  * Also run tests in Python 3.5.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 01:55:45 +0000

python-oslo.db (4.0.0-1) experimental; urgency=medium

  [ Corey Bryant ]
  * New upstream release.
  * d/gbp.conf: Change debian-branch to debian/mitaka.
  * d/control: Align requirements with upstream.
  * d/control: Update uploaders.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Dec 2015 13:41:00 +0000

python-oslo.db (2.6.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 19:32:40 +0000

python-oslo.db (2.6.0-2) experimental; urgency=medium

  * Added Python 3 support.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2015 07:58:23 +0000

python-oslo.db (2.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2015 00:20:03 +0200

python-oslo.db (2.4.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Sep 2015 08:43:12 +0000

python-oslo.db (2.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Priority: extra for transition packages.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Jul 2015 21:37:49 +0000

python-oslo.db (1.11.0-2) experimental; urgency=medium

  [ James Page ]
  * Fixup typo in transitional package description (LP: #1471561).

  [ Thomas Goirand ]
  * Rebuilt with new SQLA 1.0.6.

 -- Thomas Goirand <zigo@debian.org>  Thu, 09 Jul 2015 15:14:53 +0000

python-oslo.db (1.11.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file to use pypi.debian.net.

 -- James Page <james.page@ubuntu.com>  Wed, 10 Jun 2015 11:26:35 +0100

python-oslo.db (1.10.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release:
    - d/control: Align version requirements with upstream.
  * Re-align with Ubuntu:
    - d/control: Add Breaks/Replaces and transitional packages for
      *oslo-db in Ubuntu.

 -- James Page <james.page@ubuntu.com>  Tue, 09 Jun 2015 08:51:45 +0100

python-oslo.db (1.7.1-1) unstable; urgency=medium

  * New upstream release.
  * Reviewed (build-)depends for this release.
  * Uploading to unstable.
  * Standards-Versions: 3.9.6.

 -- Thomas Goirand <zigo@debian.org>  Sat, 07 Feb 2015 21:20:12 +0100

python-oslo.db (1.0.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Oct 2014 22:31:26 +0800

python-oslo.db (1.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Uploading to Experimental before Jessie freeze.
  * Updated (build-)depends for this release.
  * Allow the unit tests to fail because they don't work with the namespace of
    oslo :(.
  * Added PYTHONPATH=. when generating sphinx doc.

 -- Thomas Goirand <zigo@debian.org>  Fri, 19 Sep 2014 22:34:16 +0800

python-oslo.db (0.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Patches MANIFEST.in otherwise upstream setup.py is missing lots of files in
    the oslo/db/openstack folder.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Jul 2014 00:48:18 +0800

python-oslo.db (0.2.0-1) unstable; urgency=medium

  * Initial release. (Closes: #752621)

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Jun 2014 16:11:40 +0800
